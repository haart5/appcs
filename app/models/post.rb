class Post < ActiveRecord::Base
	has_many :comments
	belongs_to :user
	validates_presence_of :title
	validates_presence_of :body
	validates_length_of :body, :in => 10..400, :message => "La longitud no es valida"
end
