class Comment < ActiveRecord::Base
	belongs_to :post
	validates_presence_of :body
	validates_length_of :body, :in => 20..400, :message => "La longitud no es valida"
end
